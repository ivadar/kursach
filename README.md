# basiccalculator
TITLE:- Long Integer Calculator
NAME:- Daria Ivanchenko

Description:

------>	My LICal basic calculator does the basic operations such as 
----> multiplication
----> addition
----> subtraction
----> division
----> modulus

------> My calculator supports numbers in different number systems in the 
range [2:36]
	
------> My calculator can read expressions from a file and write them to a 
file

------> Operations in it are performed sequentially
