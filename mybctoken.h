#ifndef __MYBCTOKEN_H
#define __MYBCTOKEN_H
#define OPERAND 10 
#define OPERATOR 20
#define VARIABLE 30
#define	END	40
#define ERROR	50
#define PREINC	60
#define POSINC	70
#define PREDEC	80
#define POSDEC	90
typedef struct token {
	char op;
	int dec;
	int type; /* type takes values OPERAND/OPERATOR/END/*/	
	list *l; 
	int flag;
	char var;
	
}token;
token *getnext(char *, int *);
enum states { SPC, DIG, OPR, VAR, STOP, ERR };
#endif
