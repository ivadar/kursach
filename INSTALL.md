The source files of the calculator are located at https://gitlab.com/ivadar/kursach. To upload them to your
computer, you need to copy the link provided by the site, in the terminal type the command git clone << link >>.
This will bring up the kursach directory.
This directory contains files with the extensions .h and .c - these are the source files, there is also a Makefile license, files README.md, INSTALL.md, AUTHOR.md, which contain information about the author of the project, instructions for installation and application information. To install the application, you must first run the ./configure script, then make compile and then make install. When running the ./configure script, the user will be prompted for the path to certificate installation, path for executable files. Next, the files will be compiled and an executable file will be created LICal. Make install will create a directory for the .log files. The executable will go to the user-specified or default directory.
Other goals of the makefile:
----> compile (default target) - compiles the entire project.
----> user_man - Compiles a user manual.
----> in_instruct - Compiles installation instructions.
----> an_instruct - compiles an instruction for the aircraft analysis tool.
----> license - compiles an aircraft distribution license.
----> docs - depends on user_man in_instruct license
----> configure - the configuration script is launched and the installation.conf instruction file is generated 
for the installation.
----> all - Depends on compilation, setup and installation goals.
----> clear_tmp - cleans up temporary files generated during compilation.
----> clear_config - removes the setup configuration file.
----> clear_compile - removes compiled BC files.
----> clear - will execute clear_tmp clear_config uninstall - for the installed aircraft, all installed files 
are deleted, except for save files.
----> remove - removes all installed files, except save files, and also g files.
----> remove_all - execute ove removes save directories.