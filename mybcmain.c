#include "mybcstacks.h"
#include "mybclist.h"
#include "mybcinfix.h"
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <argp.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#define CLOCKS_PER_SEC  ((__clock_t) 1000000)
#define COUNT 2048 

char * settime(struct tm *u)
{
  char s[40];
  char *tmp;
  for (int i = 0; i<40; i++) s[i] = 0;
  strftime(s, 40, "%d_%m_%Y_%H_%M_%S", u);
  tmp = (char*)malloc(sizeof(s));
  strcpy(tmp, s);
  return(tmp);
}

int main(int argc, char *argv[]) 
{
	clock_t start, finish;
	start = clock();
	struct tm *u;
	char *f;
	const time_t timer = time(NULL);
 	u = localtime(&timer);
	f = settime(u);
	char file_log[50];
    char *_log = ".log";
    memset(file_log, 0, 50);
    strcpy(file_log, "/var/log/LICal/LICal_");
	strcat(file_log, f);
    strcat(file_log, _log);
	FILE *logs = fopen(file_log, "a+");
	fprintf(logs, "Date and time, when programm start: %s\n", f);
	fprintf(logs, "ID: %d\n", getuid());
	fflush(logs);
	FILE *input_file;
	FILE *output_file;
	int arg = 0;
	char str[COUNT];
    char *line = NULL;
    size_t len = 0;	
    char input_file_name[20];
	char output_file_name[20];
	int x;
    list *ans;
	ans = (list *)malloc(sizeof(list));
	init(ans);
	if(argc == 2 && strcmp(argv[1], "-h") == 0) {
		printf("MODE:- HELP\n");
		printf("\nUsage: LICal [option]\n");
		printf("\t-h\t--help\t\t\tprint this help and exit\n");
		printf("\t-s\t--silent\t\t.log files are not created\n");
		printf("\t-i\t--information\t\tprints iformation about student\n");
		printf("\t-o\t--out file\t\ttransfer the file with which the program will write a result \n");
		printf("\t-f\t--file\t\t\ttransfer the file with which the program will work\n\n");
		finish = clock();
		fprintf(logs, "Time of work: %ld\n", (start-finish)/CLOCKS_PER_SEC);
		fprintf(logs, "Code finish is 0\n");
		exit(0);	
	}
	else if(argc == 2 && strcmp(argv[1], "-i") == 0) {
		arg = 2;
		printf("Ivanchenko Darya Vladimirovna\n");
		printf("Group M8O-110B-80\n");
		printf("Receiving teacher: Kartashov Eduard Mikhailovich\n");
		printf("Moscow Aviation Institute (National Research University). Departments 813\n");
	}
	else if(argc == 2 && strcmp(argv[1], "-s") == 0) {
		printf("MODE:- QUIET\n");
		arg = 4;
	}
	else if(argc >= 2 && strcmp(argv[1], "-f") == 0) {
		if (argc == 2)
		{
            arg = 5;
            
            do
            {
                printf("Input name file for read: ");
                fgets(input_file_name, 20, stdin);
                input_file_name[strlen(input_file_name) - 1] = '\0';
                if ((input_file = fopen(input_file_name, "r+")) == NULL)
                    printf("Error, file open error, repeat please\n");
            } while ((input_file = fopen(input_file_name, "r+")) == NULL);
		} else if(argc == 3) {
            strcpy(input_file_name,argv[2]);
            if ((input_file = fopen(input_file_name, "r+")) == NULL)
            {
                printf("Error, file open error, repeat please\n");
                return 2;
            }
        }
        fprintf(logs, "File: %s\n", input_file_name);
        fflush(logs);
        while(getline(&line,&len,input_file) != -1)
        {
            line[strlen(line) - 1] = '\0';
            ans = infix(line);
            traverse(ans, arg, NULL);
            printf("\n");
            return 0;
        }
	}
	
	else if(argc >= 2 && strcmp(argv[1], "-o") == 0)
	{
		if (argc == 2)
		{
    	arg = 0;
		do
		{
			printf("Input name file for read: ");
			fgets(output_file_name, 20, stdin);
			output_file_name[strlen(output_file_name) - 1] = '\0';
			if ((output_file = fopen(output_file_name, "w+")) == NULL)
				printf("Error, file open error, repeat please\n");
		} while ((output_file = fopen(output_file_name, "w+")) == NULL);
		fprintf(logs, "File: %s\n", output_file_name);
		}
		else if(argc == 3) {
			strcpy(output_file_name,argv[2]);
            if ((output_file = fopen(output_file_name, "w+")) == NULL)
            {
                printf("Error, file open error, repeat please\n");
                return 2;
            }
		}
	}
	else if(argc == 1) {
		arg = 0;
		printf("I`m ready\n");
	}
	if (argc >= 2 && strcmp(argv[1], "-s") != 0)
		fprintf(logs, "Starting programm\n");
	if (argc >= 2 && strcmp(argv[1], "-s") != 0)
		fprintf(logs, "Starting read string\n");
	while((x = readline(str, COUNT))) {
		ans = infix(str);
		if(ans == NULL) {
			fprintf(stderr, "Erorr in expression\n");
			fprintf(logs, "Erorr in expression\n");
		}  
		else {
			if(arg != 5) {
				if ((argc >= 2 && strcmp(argv[1], "-o") == 0))
				{
					output_file = fopen(output_file_name, "w+");
					fprintf(output_file, "answer = ");
					traverse(ans, arg, output_file);
					printf("\n");
				}
				else {
					printf("answer = ");
					traverse(ans, arg, NULL);
					printf("\n");
				}
		}
	}
	}
	int i, j;
	for(i = 0; i < 26; i++)
		for(j = 0; j < 10; j++)
			free(L[i][j]);
	if (argc >= 2 && strcmp(argv[1], "-s") != 0)
		fprintf(logs, "Programm finish\n");
	finish = clock();
	fprintf(logs, "Time of work: %ld\n", (start-finish)/CLOCKS_PER_SEC);
	fprintf(logs, "Code finish is 0\n");
return 0;
}