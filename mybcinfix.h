#ifndef __MYBCINFIX_H
#define __MYBCINFIX_H
#include "mybcstacks.h"
#include "mybclist.h"
#include "mybcbignum.h"
int precedence(char op);
int readline(char *arr, int len);
list *infix(char *str);
static list *L[26][10];

#endif
