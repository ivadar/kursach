include data/install.conf
.PHONY: compile user_man in_instruct an_instruct license docs configure install all clear_tmp clear_config clear_compile clear uninstall remove remove_all 

compile:mybcmain.o mybcstacks.o mybclist.o mybcbignum.o mybctoken.o mybcinfix.o 
	gcc -g mybcmain.o mybcstacks.o mybclist.o mybcbignum.o mybctoken.o mybcinfix.o -o LICal -lm
mybcmain.o: mybcmain.c mybcstacks.h mybclist.h mybcinfix.h
	gcc -g -c mybcmain.c -Wall
mybcstacks.o: mybcstacks.c mybcstacks.h mybclist.h
	gcc -g -c mybcstacks.c -Wall
mybclist.o: mybclist.c mybclist.h mybcstacks.h
	gcc -g -c mybclist.c -Wall
mybcbignum.o: mybcbignum.c mybclist.h mybcbignum.h mybcstacks.h
	gcc -g -c mybcbignum.c -Wall
mybctoken.o: mybctoken.c mybctoken.h mybclist.h
	gcc -g -c mybctoken.c -Wall
mybcinfix.o: mybcinfix.c mybcinfix.h mybcstacks.h mybclist.h mybcbignum.h mybctoken.h
	gcc -g -c mybcinfix.c -Wall
user_man:
	pdflatex -synctex=1 -interaction=nonstopmode data/user_man.tex
in_instruct: 
	pdflatex -synctex=1 -interaction=nonstopmode data/in_instruct.tex
an_instruct:
	pdflatex -synctex=1 -interaction=nonstopmode data/an_instruct.tex
license:
	pdflatex -synctex=1 -interaction=nonstopmode data/license.tex
docs: user_man in_instruct license
install: LICal 
	cp -p LICal $(BIN_PATH)
	mkdir -p ~/LICal
	sudo mkdir -p /var/log/LICal
	sudo chmod 777 /var/log/LICal
all: compile install docs
clear_tmp: 
	rm -f *.o *.aux *.gz
clear_config:
	rm -f *.conf
clear_compile: clear_tmp
	rm -f LICal
clear: clear_tmp clear_config
uninstall:
	rm -rf ~/LICal
	rm -f $(BIN_PATH)/LICal
remove: uninstall
	rm -f /var/log/LICal/LICal.log
remove_all: remove
	rm -rf ~/LICal

